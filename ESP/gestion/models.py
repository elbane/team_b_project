from django.db import models
from django.core.validators import MinValueValidator

class Departement(models.Model):
    nom = models.CharField(max_length=50)
    code = models.CharField(max_length=10)

    def __str__(self):
        return self.nom

class ElementModule(models.Model):
    code = models.CharField(max_length=10,unique=True)
    nom = models.CharField(max_length=100,unique=True)
    departement = models.ForeignKey(Departement, on_delete=models.CASCADE)
    coefficient = models.IntegerField(validators=[MinValueValidator(1)])
    credit = models.IntegerField(validators=[MinValueValidator(1)])

    def __str__(self):
        return self.nom


class Etudiant(models.Model):
    nom = models.CharField(max_length=50)
    prenom = models.CharField(max_length=50)
    date_de_naissance = models.DateField(help_text = "\nPlease use the following format: <em>YYYY-MM-DD</em>.",)
    lieu_de_naissance = models.CharField(max_length=100)
    pays_de_naissance = models.CharField(max_length=100)
    departement = models.ForeignKey(Departement, on_delete=models.CASCADE, blank=True)
    voie_acces = models.CharField(max_length=50, choices=[('voie1', 'Voie 1'), ('voie2', 'Voie 2'), ('reorientation', 'Réorientation')])
    matricule = models.IntegerField(validators=[MinValueValidator(10000)], unique=True, blank=True)
    nationalite = models.CharField(max_length=50)
    identifiant_national = models.IntegerField(null=True)

    statut = models.CharField(max_length=10, choices= [
        ('Etudiant', 'Etudiant'),
        ('dipme', 'Diplômé'),
        ('renvoyé', 'Renvoyé'),
        ('DD', 'DD'),
        ('échange', 'Échange'),
        ('cénsure', 'Césure'),
        ('abandon', 'Abandon'),
    ])
    annee_inscription = models.IntegerField(blank=True)
   
   
    def __str__(self):
    	return self.prenom+" "+self.nom
    def get_absolute_url(self):
        return reverse('etudiant_detail', args=[str(self.id)])


class Note(models.Model):
    etudiant = models.ForeignKey(Etudiant, on_delete=models.CASCADE)
    departement = models.ForeignKey(Departement, on_delete=models.CASCADE)
    element_module = models.ForeignKey(ElementModule, on_delete=models.CASCADE)
    note = models.DecimalField(max_digits=5, decimal_places=2,unique=True)

    def __str__(self):
        return f"Note de {self.etudiant} pour {self.element_module}"

