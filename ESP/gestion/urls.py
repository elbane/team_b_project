from django.urls import path
from . import views
urlpatterns = [
 	path('',views.home ,name='home'), 
 	path('gestion_etudiant.html/',views.gestion_etudiant ,name='gestion_etudiant'),
 	path('gestion_departement.html/',views.gestion_departement ,name='gestion_departement'),
 	path('gestion_modules.html/',views.gestion_modules ,name='gestion_modules'),
            path('listEtudiants/', views.EtudiantsList.as_view(), name= 'index' ),
            path('listDepartement/', views.DepartementsList.as_view(), name= 'departementlist' ),
    path('ajouterEtudiant/', views.EtudiantsCreate.as_view(), name= 'add' ),
    path('etudiant/<int:matricule>/', views.EtudiantDetailView.as_view(), name='etudiant_detail'),
    path('etudiant/<int:matricule>/modifier/', views.EtudiantUpdate.as_view(), name='etudiant-update'),
    path('etudiant/delete/<int:pk>/', views.EtudiantDeleteView.as_view(), name='etudiant-delete'),
    #path('etudiant/search/', views.EtudiantSearchView.as_view(), name='rechercher_etudiant'),
    path('etudiant/recherche/', views.EtudiantRechercheView.as_view(), name='recherche_etudiant'),
path('departement/ajouter/', views.DepartementCreate.as_view(), name='departement_create'),
    path('departement/modifier/<str:nom>/', views.DepartementUpdate.as_view(), name='departement_update'),
    path('departement/supprimer/<str:nom>/', views.DepartementDelete.as_view(), name='departement_delete'),
    path('departement/<str:nom>/', views.DepartementEtudiants.as_view(), name='departement_etudiants'), 
    
    path('elementmodule/ajouter/', views.ElementModuleCreate.as_view(), name='elementmodule_create'),
    path('elementmodule/modifier/<int:pk>/', views.ElementModuleUpdate.as_view(), name='elementmodule_update'),
path('elementmodule/supprimer/<int:pk>/', views.ElementModuleDelete.as_view(), name='elementmodule_delete'),

     path('note/ajouter/', views.NoteCreate.as_view(), name='note_create'),
    path('note/modifier/<int:pk>/', views.NoteUpdate.as_view(), name='note_update'),
    path('note/supprimer/<int:pk>/', views.NoteDelete.as_view(), name='note_delete'),
    path('note/liste/<str:departemen>', views.NoteList.as_view(), name='note_list'),

]

