from django import forms
from .models import Departement, Etudiant

class EtudiantsCreateForm(forms.ModelForm):
    departement = forms.ModelChoiceField(queryset=Departement.objects.all(), empty_label=None)

    class Meta:
        model = Etudiant
        fields = '__all__'
