

Certificat d'Inscription des Étudiants

Le soussigné, représentant de l'école d'ingénierie ESP, 
certifie par la présente que :

Nom de l'étudiant : [NOM de l'étudiant]
Prénom de l'étudiant : [Prénom de l'étudiant]
Date de naissance : [Date de naissance]
Lieu de naissance : [Lieu de naissance]
Département : [Département (optionnel)]
Voie d'accès : [Voie d'accès]
Matricule : [Matricule]
Nationalité : [Nationalité]
Identifiant national : [NNI ou Numéro de passeport]
Statut : [Statut]

L'étudiant(e) susmentionné(e) a été officiellement admis(e) dans notre établissement pour 
le semestre scolaire [Indiquer le semestre scolaire].

Cette inscription est valide à partir de la date de délivrance du présent certificat
et est soumise aux règles et réglementations en vigueur au sein de notre institution.
L'étudiant(e) est tenu(e) de se conformer à toutes les politiques académiques et administratives de l'école.

Ce certificat d'inscription est délivré à des fins administratives 
et peut être utilisé comme preuve d'inscription auprès des autorités compétentes, 
des organismes gouvernementaux et des tiers, selon les besoins.

Fait à [Lieu de délivrance], le [Date de délivrance].

[Signature de l'autorité compétente]
[Nom de l'autorité compétente]
[Titre de l'autorité compétente]
[École d'Ingénierie ESP]
