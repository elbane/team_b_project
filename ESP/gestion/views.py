from django.shortcuts import render,HttpResponse
from django.views.generic import *
from .models import *
from django.urls import reverse_lazy
from reportlab.pdfgen import canvas
from .forms import EtudiantsCreateForm
#from docx import Document
from reportlab.lib.pagesizes import letter
# Create your views here.
def home(request):
	return render(request,'gestion/home.html')
def gestion_etudiant(request):
	return render(request,'gestion/gestion_etudiant.html')
def gestion_departement(request):
	return render(request,'gestion/gestion de departement.html')
def gestion_modules(request):
	return render(request,'gestion/gestion de elements modules.html')
class DepartementCreate(CreateView):
    model = Departement
    fields = ['nom', 'code']
    success_url = reverse_lazy('home')
    template_name = 'gestion/departement_create.html'

class DepartementUpdate(UpdateView):
    model = Departement
    fields = ['nom', 'code']
    success_url = reverse_lazy('home')
    template_name = 'gestion/departement_update.html'
    def get_object(self, queryset=None):
        nom = self.kwargs.get('nom')
        return self.model.objects.get(nom=nom)
class DepartementDelete(DeleteView):
    model = Departement
    success_url = reverse_lazy('home')
    template_name = 'gestion/departement_confirm_delete.html'
    def get_object(self, queryset=None):
        nom = self.kwargs.get('nom')
        return self.model.objects.get(nom=nom)
class DepartementEtudiants(ListView):
    model = Etudiant
    template_name = 'gestion/liste_etudiants_departement.html'
    context_object_name = 'etudiants'
    def get_object(self, queryset=None):
        context = super().get_context_data(**kwargs)
        etudiants = self.object
        modules = ElementModule.objects.filter(departement=nom)
        context['modules'] = modules
        return context
class DepartementsList(ListView):
    model = Departement
    template_name='gestion/liste_departements.html'
    context_object_name = 'departements'
    


    def get_object(self, queryset=None):
        nom = self.kwargs.get('nom')
        return self.model.objects.get(nom=nom)

	
class EtudiantsCreate(CreateView):
    model = Etudiant
    template_name = 'gestion/inscription_Etudiant.html'
    form_class = EtudiantsCreateForm
    success_url = '../ajouterEtudiant'

class EtudiantsList(ListView):
    model = Etudiant
    template_name='gestion/liste_etudiants.html'
    context_object_name = 'etudiants'
class EtudiantDetailView(DetailView):
    model = Etudiant
    template_name = 'gestion/etudiant_detail.html'
    context_object_name = 'etudiant'


    def generate_certificat_inscription(self):
	    etudiant = self.get_object()

	    with open("gestion/templates/template_inscription.txt", "r") as file:
	    	contenu = file.read()
	    contenu_rempli = contenu.replace("[NOM de l'étudiant]", etudiant.nom)
	    contenu_rempli = contenu_rempli.replace("[Prénom de l'étudiant]", etudiant.prenom)
	    contenu_rempli = contenu_rempli.replace("[Date de naissance]", str(etudiant.date_de_naissance))
	    contenu_rempli = contenu_rempli.replace("[Lieu de naissance]", str(etudiant.lieu_de_naissance))
	    contenu_rempli = contenu_rempli.replace("[Département (optionnel)]", str(etudiant.departement))
	    contenu_rempli = contenu_rempli.replace("[Voie d'accès]", etudiant.voie_acces)
	    contenu_rempli = contenu_rempli.replace("[Matricule]", str(etudiant.matricule))
	    contenu_rempli = contenu_rempli.replace("[Nationalité]", etudiant.nationalite)
	    contenu_rempli = contenu_rempli.replace("[NNI ou Numéro de passeport]", str(etudiant.identifiant_national))
	    contenu_rempli = contenu_rempli.replace("[Statut]", etudiant.statut)

	    # Générer le PDF avec le texte rempli
	    #contenu_rempli = contenu_rempli[1:]
	    pdf_filename = f"certificat_inscriptionde_{etudiant.matricule}.pdf"
	    pdf_canvas = canvas.Canvas(pdf_filename,pagesize=letter)
	    pdf_canvas.setFont("Helvetica", 10)
	    logo_path = "static/images/logo.jpg"
	    pdf_canvas.drawImage(logo_path, 250, 700, width=100, height=100)
	    
	    lignes = contenu_rempli.split("\n")
	    y = 700
	    for ligne in lignes:
        	    pdf_canvas.drawString(100, y, ligne)
        	    y -= 15
	    pdf_canvas.showPage()
	    pdf_canvas.save()

	    with open(pdf_filename, 'rb') as file:
	    	response = HttpResponse(file.read(), content_type='application/pdf')
	    	response['Content-Disposition'] = f'attachment; filename="{pdf_filename}"'

	    return response

    def get(self, request, *args, **kwargs):
        if 'download' in request.GET:
            # Télécharge le certificat d'inscription
            return self.generate_certificat_inscription()
        else:
            # Affiche les détails de l'étudiant
            return super().get(request, *args, **kwargs)
        
    def get_object(self, queryset=None):
        matricule = self.kwargs.get('matricule')
        return self.model.objects.get(matricule=matricule)
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        etudiant = self.object
        modules = ElementModule.objects.filter(departement=etudiant.departement)
        notes = Note.objects.filter(etudiant=etudiant)
        context['notes'] = notes
        context['modules'] = modules
        return context
class EtudiantUpdate(UpdateView):
    model = Etudiant
    template_name = 'gestion/etudiant_update.html'
    fields = '__all__'
    success_url = '../'
    def get_object(self, queryset=None):
        matricule = self.kwargs.get('matricule')
        return self.model.objects.get(matricule=matricule)
class EtudiantDeleteView(DeleteView):
    model = Etudiant
    #template_name = 'gestion/etudiant-delete.html'
    success_url = reverse_lazy('home')
    	
class EtudiantRechercheView(ListView):
    model = Etudiant
    template_name = 'gestion/recherche_etudiant.html'  # Le nom du template de recherche

    def get_queryset(self):
        queryset = super().get_queryset()

        # Récupérer les paramètres de recherche depuis la requête GET
        nni = self.request.GET.get('nni')
        matricule = self.request.GET.get('matricule')
        date_naissance = self.request.GET.get('date_naissance')
        nom = self.request.GET.get('nom')

        # Appliquer les filtres de recherche aux étudiants
        if nni:
            queryset = queryset.filter(identifiant_national=nni)
            queryset = queryset.filter(identifiant_national__contains =nni)
        if matricule:
            queryset = queryset.filter(matricule__contains =matricule)
        if date_naissance:
            queryset = queryset.filter(date_de_naissance__contains =date_naissance)
        if nom:
            queryset = queryset.filter(nom__icontains = nom)

        return queryset

class ElementModuleCreate(CreateView):
    model = ElementModule
    template_name = 'gestion/elementmodule_create.html'
    fields = ['code', 'nom', 'departement', 'coefficient', 'credit']
    success_url = reverse_lazy('home')

class ElementModuleUpdate(UpdateView):
    model = ElementModule
    template_name = 'gestion/elementmodule_update.html'
    fields = ['code', 'nom', 'departement', 'coefficient', 'credit']
    success_url = reverse_lazy('home')

class ElementModuleDelete(DeleteView):
    model = ElementModule
    template_name = 'gestion/elementmodule_confirm_delete.html'
    success_url = reverse_lazy('home')

class NoteCreate(CreateView):
    model = Note
    template_name = 'gestion/note_create.html'
    fields = ['etudiant', 'departement', 'element_module', 'note']
    success_url = reverse_lazy('home')

class NoteUpdate(UpdateView):
    model = Note
    template_name = 'gestion/note_update.html'
    fields = ['etudiant', 'departement', 'element_module', 'note']
    success_url = reverse_lazy('home')

class NoteDelete(DeleteView):
    model = Note
    template_name = 'gestion/note_confirm_delete.html'
    success_url = reverse_lazy('home')

class NoteList(ListView):
    model = Note
    template_name = 'gestion/note_list.html'
    context_object_name = 'notes'
    def get_object(self, queryset=None):
        	departemen = self.kwargs.get('departemen')
        	return self.model.objects.get(departemen=departemen)
